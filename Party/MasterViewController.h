//
//  MasterViewController.h
//  Party
//
//  Created by Hanumant S on 12/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartyCollectionView.h"
#import "MWPhotoBrowser.h"



@interface MasterViewController : UIViewController<PhotoDownloadDelegate, MWPhotoBrowserDelegate>


@end
