//
//  PartyCollectionView.h
//  Party
//
//  Created by Hanumant S on 12/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import <UIKit/UIKit.h>


// Custom delegate to handle the cell item selection from collection view.
@protocol PhotoDownloadDelegate <NSObject>

- (void)didPageReachToEnd: (NSInteger)imageIndex;
- (void)didSelectItemAtIndex: (NSInteger)imageIndex;

@end



@interface PartyCollectionView : UICollectionView <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property(weak, nonatomic) id<PhotoDownloadDelegate> downloadDelegate;
@property(strong, atomic) NSMutableArray *imageResourceArray;

@end
