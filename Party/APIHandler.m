//
//  Flickr.m
//  Party
//
//  Created by Hanumant S on 13/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import "APIHandler.h"

@implementation APIHandler

+ (void)httpRequest:(NSURLRequest *)request callBack:(void (^)(id , NSString *))responseHandler {
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        // Handle data and error.
        if (error == nil) {
            
            // Convert Data object into Dictionary.
            NSDictionary *jsonDictionary =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            responseHandler(jsonDictionary, nil);
            
        } else {
            
            responseHandler(error.localizedDescription, nil);
        }
        
    }];
    
    [dataTask resume];
}



@end
