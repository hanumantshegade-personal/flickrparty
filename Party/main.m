//
//  main.m
//  Party
//
//  Created by Hanumant S on 11/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
