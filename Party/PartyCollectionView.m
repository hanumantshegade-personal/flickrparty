//
//  PartyCollectionView.m
//  Party
//
//  Created by Hanumant S on 12/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import "PartyCollectionView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PartyCollectionView {
    float cellWidth;
}

static NSString * const reuseIdentifier = @"Cell";

- (id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    if (self = [super initWithFrame:frame collectionViewLayout:layout]) {
    
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = [UIColor colorWithRed:22/255.0 green:188/255.0 blue:156/255.0 alpha:1.0];
        [self registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
        cellWidth = frame.size.width;
        
    }
    
    return self;
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [_imageResourceArray count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImageView *tagImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, 250)];
    cell.contentMode = UIViewContentModeScaleAspectFill;
    NSURL *imageURL = [_imageResourceArray objectAtIndex:indexPath.row];
    [cell addSubview:tagImageView];
    
    // Set autoresizing for image view.
    tagImageView.autoresizingMask =
    ( UIViewAutoresizingFlexibleBottomMargin
     | UIViewAutoresizingFlexibleHeight
     | UIViewAutoresizingFlexibleLeftMargin
     | UIViewAutoresizingFlexibleRightMargin
     | UIViewAutoresizingFlexibleTopMargin
     | UIViewAutoresizingFlexibleWidth );

    // Download the images using the SDImageManager
    [tagImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    // Download the next page of image after every 480 images.
    if (indexPath.row % 480 == 0) {
        [_downloadDelegate didPageReachToEnd:indexPath.row];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    [_downloadDelegate didSelectItemAtIndex:indexPath.row];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(cellWidth, 250);
}


@end

