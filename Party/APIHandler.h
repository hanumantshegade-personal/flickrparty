//
//  Flickr.h
//  Party
//
//  Created by Hanumant S on 13/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIHandler : NSObject

// API request method, with call back of success and error response.

+ (void)httpRequest: (NSURLRequest *)request callBack:(void(^)(id, NSString *)) handler;

@end
