//
//  MasterViewController.m
//  Party
//
//  Created by Hanumant S on 12/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import "MasterViewController.h"
#import "APIHandler.h"

#define API_KEY     @"ab2db08cc7870403b29ac1b577c5e8da"
#define SECRET      @"7f31abc107e95224"

@interface MasterViewController () {
    PartyCollectionView *collectionView;
    NSMutableArray *photos;
}

@end


@implementation MasterViewController


#pragma mark - Init methods.

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize collection view.
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    collectionView = [[PartyCollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    collectionView.downloadDelegate = self;
    [self.view addSubview:collectionView];
    photos = [NSMutableArray array];
    
    // Set the flickr image tag.
    [self setFlickrCallByTag:@"party"];
}

- (void)setFlickrCallByTag: (NSString *)tag {
    
    // Request for 100 images per page from photo search api.
    NSString *urlString =
    [NSString stringWithFormat:
     @"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&tags=%@&per_page=500&format=json&nojsoncallback=1", API_KEY, tag];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    // Setup and start async download
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url];
    [APIHandler httpRequest:request callBack:^(id data, NSString *error) {
        
        NSArray *responseArray = [data valueForKeyPath:@"photos.photo"];
        collectionView.imageResourceArray = [[NSMutableArray alloc] init];
        NSString *size = @"n";
        
        for (id dictionary in responseArray) {
            // Prepare image url
            NSString *urlString = [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_%@.jpg",
                                   [dictionary valueForKey:@"farm"],
                                   [dictionary valueForKey:@"server"],
                                   [dictionary valueForKey:@"id"],
                                   [dictionary valueForKey:@"secret"], size];
            
            NSURL *imageURL = [NSURL URLWithString:urlString];
            
            [collectionView.imageResourceArray addObject:imageURL];
            [photos addObject:[MWPhoto photoWithURL:imageURL]];
        }
        
        // Reload collection view after receving the data.
        [self performSelectorOnMainThread:@selector(reloadCollectionView) withObject:self waitUntilDone:nil];
    }];


}

- (void)reloadCollectionView {
    [collectionView reloadData];
}


#pragma mark - MWPhotoBrowserDelegate Methods

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return [photos count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count) {
        return [photos objectAtIndex:index];
    }
    
    return nil;
}


#pragma mark - PhotoDownloadDelegate Method

- (void)didSelectItemAtIndex:(NSInteger)imageIndex {
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.zoomPhotosToFill = YES;
    browser.displayActionButton = NO;
    browser.displaySelectionButtons = NO;
    browser.displayNavArrows = YES;
    [browser setCurrentPhotoIndex:imageIndex];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:browser];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)didPageReachToEnd:(NSInteger)imageIndex {
    // ToDo:- Handle Pagination
}



@end
