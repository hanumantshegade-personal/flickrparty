//
//  AppDelegate.h
//  Party
//
//  Created by Hanumant S on 11/12/15.
//  Copyright © 2015 Hanumant S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

